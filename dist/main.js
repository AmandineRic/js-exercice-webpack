/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/Character.js":
/*!**************************!*\
  !*** ./src/Character.js ***!
  \**************************/
/*! exports provided: Character */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Character", function() { return Character; });
/* harmony import */ var _index__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./index */ "./src/index.js");
/* harmony import */ var _controls_Item__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./controls/Item */ "./src/controls/Item.js");



/**
 * Pour que cette classe soit importable dans un autre
 * fichier, on doit l'indiquer avec le mot clef export
 */
class Character {
  /**
   * @param {string} name
   */
  constructor(name) {
    this.name = name;
    this.health = 100;
    this.hunger = 0;
    this.position = {
      x: 0,
      y: 0
    };
    /**
     * @type Element
     */
    this.element = null;
    /**
     * @type Item[]
     */
    this.backpack = [];
  }
  /**
   * Méthode qui déplace le personnage dans une direction donnée
   * @param {string} direction la direction dans laquelle va le personnage (doit être up, down, left ou right)
   */
  move(direction) {
    // if (direction === 'up') {
    //     this.position.y--;
    // }
    // if (direction === 'down') {
    //     this.position.y++;
    // }
    // if (direction === 'left') {
    //     this.position.x--;
    // }
    // if (direction === 'right') {
    //     this.position.x++;
    // }
    switch (direction) {
      case "up":
        this.position.y--;
        break;
      case "down":
        this.position.y++;
        break;
      case "left":
        this.position.x--;
        break;
      case "right":
        this.position.x++;
        break;
    }
    this.metabolism();
    this.draw();
  }

  metabolism() {
    if (this.hunger < 100) {
      this.hunger += 2;
    } else {
      this.health--;
    }
  }

  /**
   * Méthode qui met un item dans l'sac
   * @param {Item} item l'item à mettre dans le sac
   */
  takeItem(item) {
    if (!item) {
      for (const iterator of _index__WEBPACK_IMPORTED_MODULE_0__["gameObjects"]) {
        if (iterator instanceof _controls_Item__WEBPACK_IMPORTED_MODULE_1__["Item"]) {
          if (
            this.position.x === iterator.position.x &&
            this.position.y === iterator.position.y
          ) {
            item = iterator;
            break;
          }
        }
      }
      if (item) {
        this.backpack.push(item);
        this.position.x = null;
        this.position.y = null;
        item.element.remove();
      }
    }
  }
  /**
   * 
   * @param {Item} item 
   */
  useItem(item){
   alert(item.use(this));
  }

  /**
   * Méthode qui crée l'élément HTML du personnage
   * @returns {Element} l'élément HTML représentant le personnage
   */
  draw() {
    if (!this.element) {
      this.element = document.createElement("div");
    }
    let div = this.element;
    div.innerHTML = "";

    div.classList.add("character-style");

    let pName = document.createElement("p");
    pName.textContent = "Name : " + this.name;
    div.appendChild(pName);
    let pHealth = document.createElement("p");
    pHealth.textContent = "Health : " + this.health;
    div.appendChild(pHealth);
    let pHunger = document.createElement("p");
    pHunger.textContent = "Hunger : " + this.hunger;
    div.appendChild(pHunger);

    div.style.top = this.position.y * _index__WEBPACK_IMPORTED_MODULE_0__["tileSize"] + "px";
    div.style.left = this.position.x * _index__WEBPACK_IMPORTED_MODULE_0__["tileSize"] + "px";

    return div;
  }
}


/***/ }),

/***/ "./src/controls/ArrowKey.js":
/*!**********************************!*\
  !*** ./src/controls/ArrowKey.js ***!
  \**********************************/
/*! exports provided: ArrowKey */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ArrowKey", function() { return ArrowKey; });
/* harmony import */ var _Character__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../Character */ "./src/Character.js");


class ArrowKey {
  /**
   *
   * @param {Character} character le personnage auquel on veut assigner des contrôles au clavier
   */
  constructor(character) {
    this.character = character;
  }

  init() {
    /**
     * On ajoute un event au keydown sur la window (sur le document ça
     * marche aussi). On fait attention à faire une fat arrow function
     * pour conserver la valeur du this à l'intérieur de l'event
     */
    window.addEventListener("keydown", event => {
      //Selon la touche qui est pressée, on déclenche la méthode
      //move avec une valeur ou une autre (techniquement on pourrait
      //refactoriser ça un peu mieux, mais ça le fera)
      if (event.key === "ArrowUp") {
        this.character.move("up");
      }
      if (event.key === "ArrowDown") {
        this.character.move("down");
      }
      if (event.key === "ArrowLeft") {
        this.character.move("left");
      }
      if (event.key === "ArrowRight") {
        this.character.move("right");
      }
      if (event.key === " ") {
        this.character.takeItem();
      }
    });
  }
}


/***/ }),

/***/ "./src/controls/Item.js":
/*!******************************!*\
  !*** ./src/controls/Item.js ***!
  \******************************/
/*! exports provided: Item */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Item", function() { return Item; });
/* harmony import */ var _Character__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../Character */ "./src/Character.js");
/* harmony import */ var _index__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../index */ "./src/index.js");



class Item {
  /**
   *
   * @param {string} name le nom de l'item
   * @param {number} x position x de l'item
   * @param {number} y position y de l'item
   * @param {string} cssClass la classe css représentant cet objet
   */
  constructor(name, x, y, cssClass = "default-class") {
    this.name = name;
    this.position = {
      x,
      y
    };
    this.cssClass = cssClass;
    /**
     * @type Element
     */
    this.element = null;
  }
  /**
   * Méthode qui sera appelée quand on utilise l'item
   * @param {Character} character Le personnage qui utilise l'item
   * @returns {string} La chaîne de caractère indiquant l'utilisation de l'item
   */
  use(character) {
    return `${character.name} used ${this.name} !`;
  }
  /**
   *
   * @returns {Element} l'élément html représentant l'item
   */
  draw() {
    if (!this.element) {
      this.element = document.createElement("div");
    }
    let div = this.element;
    div.innerHTML = "";

    div.classList.add("item");
    div.classList.add(this.cssClass);
    div.textContent = "?";

    console.log(div);

    div.style.top = this.position.y * _index__WEBPACK_IMPORTED_MODULE_1__["tileSize"] + "px";
    div.style.left = this.position.x * _index__WEBPACK_IMPORTED_MODULE_1__["tileSize"] + "px";

    return div;
  }
}


/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! exports provided: tileSize, gameObjects */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "tileSize", function() { return tileSize; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "gameObjects", function() { return gameObjects; });
/* harmony import */ var _Character__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Character */ "./src/Character.js");
/* harmony import */ var _controls_ArrowKey__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./controls/ArrowKey */ "./src/controls/ArrowKey.js");
/* harmony import */ var _controls_Item__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./controls/Item */ "./src/controls/Item.js");
/**
 * En JS modulaire, pour pouvoir utiliser la classe Character
 * dans notre fichier index, on doit d'abord importer la
 * classe en question de son fichier
 */




const tileSize = 100;
//On fait une instance de personnage
let perso1 = new _Character__WEBPACK_IMPORTED_MODULE_0__["Character"]("Perso 1");
const gameObjects = [
  perso1,
  new _controls_Item__WEBPACK_IMPORTED_MODULE_2__["Item"]("little hat", 4, 2),
  new _controls_Item__WEBPACK_IMPORTED_MODULE_2__["Item"]("bread", 5, 5)
];
for (const iterator of gameObjects) {
  document.body.appendChild(iterator.draw());
}

perso1.useItem(new _controls_Item__WEBPACK_IMPORTED_MODULE_2__["Item"]('some stuff'));
//on fait une instance des contrôles au clavier en lui
//donnant l'instance de personnage à contrôler
let arrowKey = new _controls_ArrowKey__WEBPACK_IMPORTED_MODULE_1__["ArrowKey"](perso1);
//On initialise les contrôles au clavier
arrowKey.init();
//On fait un draw initial pour append le html du personnage où on veut


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL0NoYXJhY3Rlci5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY29udHJvbHMvQXJyb3dLZXkuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NvbnRyb2xzL0l0ZW0uanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2luZGV4LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7UUFBQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTs7O1FBR0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDBDQUEwQyxnQ0FBZ0M7UUFDMUU7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSx3REFBd0Qsa0JBQWtCO1FBQzFFO1FBQ0EsaURBQWlELGNBQWM7UUFDL0Q7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBLHlDQUF5QyxpQ0FBaUM7UUFDMUUsZ0hBQWdILG1CQUFtQixFQUFFO1FBQ3JJO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMkJBQTJCLDBCQUEwQixFQUFFO1FBQ3ZELGlDQUFpQyxlQUFlO1FBQ2hEO1FBQ0E7UUFDQTs7UUFFQTtRQUNBLHNEQUFzRCwrREFBK0Q7O1FBRXJIO1FBQ0E7OztRQUdBO1FBQ0E7Ozs7Ozs7Ozs7Ozs7QUNsRkE7QUFBQTtBQUFBO0FBQUE7QUFBZ0Q7QUFDVDs7QUFFdkM7QUFDQTtBQUNBO0FBQ0E7QUFDTztBQUNQO0FBQ0EsYUFBYSxPQUFPO0FBQ3BCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLE9BQU87QUFDcEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsYUFBYSxLQUFLO0FBQ2xCO0FBQ0E7QUFDQTtBQUNBLDZCQUE2QixrREFBVztBQUN4QyxnQ0FBZ0MsbURBQUk7QUFDcEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLEtBQUs7QUFDbEI7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLGVBQWUsUUFBUTtBQUN2QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsc0NBQXNDLCtDQUFRO0FBQzlDLHVDQUF1QywrQ0FBUTs7QUFFL0M7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7O0FDcElBO0FBQUE7QUFBQTtBQUF5Qzs7QUFFbEM7QUFDUDtBQUNBO0FBQ0EsYUFBYSxVQUFVO0FBQ3ZCO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7Ozs7Ozs7Ozs7Ozs7QUN0Q0E7QUFBQTtBQUFBO0FBQUE7QUFBeUM7QUFDTDs7QUFFN0I7QUFDUDtBQUNBO0FBQ0EsYUFBYSxPQUFPO0FBQ3BCLGFBQWEsT0FBTztBQUNwQixhQUFhLE9BQU87QUFDcEIsYUFBYSxPQUFPO0FBQ3BCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWEsVUFBVTtBQUN2QixlQUFlLE9BQU87QUFDdEI7QUFDQTtBQUNBLGNBQWMsZUFBZSxRQUFRLFVBQVU7QUFDL0M7QUFDQTtBQUNBO0FBQ0EsZUFBZSxRQUFRO0FBQ3ZCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQSxzQ0FBc0MsK0NBQVE7QUFDOUMsdUNBQXVDLCtDQUFROztBQUUvQztBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7QUNyREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUN3QztBQUNPO0FBQ1I7O0FBRWhDO0FBQ1A7QUFDQSxpQkFBaUIsb0RBQVM7QUFDbkI7QUFDUDtBQUNBLE1BQU0sbURBQUk7QUFDVixNQUFNLG1EQUFJO0FBQ1Y7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsbUJBQW1CLG1EQUFJO0FBQ3ZCO0FBQ0E7QUFDQSxtQkFBbUIsMkRBQVE7QUFDM0I7QUFDQTtBQUNBIiwiZmlsZSI6Im1haW4uanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBnZXR0ZXIgfSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbiBcdFx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG4gXHRcdH1cbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbiBcdH07XG5cbiBcdC8vIGNyZWF0ZSBhIGZha2UgbmFtZXNwYWNlIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDE6IHZhbHVlIGlzIGEgbW9kdWxlIGlkLCByZXF1aXJlIGl0XG4gXHQvLyBtb2RlICYgMjogbWVyZ2UgYWxsIHByb3BlcnRpZXMgb2YgdmFsdWUgaW50byB0aGUgbnNcbiBcdC8vIG1vZGUgJiA0OiByZXR1cm4gdmFsdWUgd2hlbiBhbHJlYWR5IG5zIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDh8MTogYmVoYXZlIGxpa2UgcmVxdWlyZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy50ID0gZnVuY3Rpb24odmFsdWUsIG1vZGUpIHtcbiBcdFx0aWYobW9kZSAmIDEpIHZhbHVlID0gX193ZWJwYWNrX3JlcXVpcmVfXyh2YWx1ZSk7XG4gXHRcdGlmKG1vZGUgJiA4KSByZXR1cm4gdmFsdWU7XG4gXHRcdGlmKChtb2RlICYgNCkgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAmJiB2YWx1ZS5fX2VzTW9kdWxlKSByZXR1cm4gdmFsdWU7XG4gXHRcdHZhciBucyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18ucihucyk7XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShucywgJ2RlZmF1bHQnLCB7IGVudW1lcmFibGU6IHRydWUsIHZhbHVlOiB2YWx1ZSB9KTtcbiBcdFx0aWYobW9kZSAmIDIgJiYgdHlwZW9mIHZhbHVlICE9ICdzdHJpbmcnKSBmb3IodmFyIGtleSBpbiB2YWx1ZSkgX193ZWJwYWNrX3JlcXVpcmVfXy5kKG5zLCBrZXksIGZ1bmN0aW9uKGtleSkgeyByZXR1cm4gdmFsdWVba2V5XTsgfS5iaW5kKG51bGwsIGtleSkpO1xuIFx0XHRyZXR1cm4gbnM7XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gXCIuL3NyYy9pbmRleC5qc1wiKTtcbiIsImltcG9ydCB7IHRpbGVTaXplLCBnYW1lT2JqZWN0cyB9IGZyb20gXCIuL2luZGV4XCI7XG5pbXBvcnQgeyBJdGVtIH0gZnJvbSBcIi4vY29udHJvbHMvSXRlbVwiO1xuXG4vKipcbiAqIFBvdXIgcXVlIGNldHRlIGNsYXNzZSBzb2l0IGltcG9ydGFibGUgZGFucyB1biBhdXRyZVxuICogZmljaGllciwgb24gZG9pdCBsJ2luZGlxdWVyIGF2ZWMgbGUgbW90IGNsZWYgZXhwb3J0XG4gKi9cbmV4cG9ydCBjbGFzcyBDaGFyYWN0ZXIge1xuICAvKipcbiAgICogQHBhcmFtIHtzdHJpbmd9IG5hbWVcbiAgICovXG4gIGNvbnN0cnVjdG9yKG5hbWUpIHtcbiAgICB0aGlzLm5hbWUgPSBuYW1lO1xuICAgIHRoaXMuaGVhbHRoID0gMTAwO1xuICAgIHRoaXMuaHVuZ2VyID0gMDtcbiAgICB0aGlzLnBvc2l0aW9uID0ge1xuICAgICAgeDogMCxcbiAgICAgIHk6IDBcbiAgICB9O1xuICAgIC8qKlxuICAgICAqIEB0eXBlIEVsZW1lbnRcbiAgICAgKi9cbiAgICB0aGlzLmVsZW1lbnQgPSBudWxsO1xuICAgIC8qKlxuICAgICAqIEB0eXBlIEl0ZW1bXVxuICAgICAqL1xuICAgIHRoaXMuYmFja3BhY2sgPSBbXTtcbiAgfVxuICAvKipcbiAgICogTcOpdGhvZGUgcXVpIGTDqXBsYWNlIGxlIHBlcnNvbm5hZ2UgZGFucyB1bmUgZGlyZWN0aW9uIGRvbm7DqWVcbiAgICogQHBhcmFtIHtzdHJpbmd9IGRpcmVjdGlvbiBsYSBkaXJlY3Rpb24gZGFucyBsYXF1ZWxsZSB2YSBsZSBwZXJzb25uYWdlIChkb2l0IMOqdHJlIHVwLCBkb3duLCBsZWZ0IG91IHJpZ2h0KVxuICAgKi9cbiAgbW92ZShkaXJlY3Rpb24pIHtcbiAgICAvLyBpZiAoZGlyZWN0aW9uID09PSAndXAnKSB7XG4gICAgLy8gICAgIHRoaXMucG9zaXRpb24ueS0tO1xuICAgIC8vIH1cbiAgICAvLyBpZiAoZGlyZWN0aW9uID09PSAnZG93bicpIHtcbiAgICAvLyAgICAgdGhpcy5wb3NpdGlvbi55Kys7XG4gICAgLy8gfVxuICAgIC8vIGlmIChkaXJlY3Rpb24gPT09ICdsZWZ0Jykge1xuICAgIC8vICAgICB0aGlzLnBvc2l0aW9uLngtLTtcbiAgICAvLyB9XG4gICAgLy8gaWYgKGRpcmVjdGlvbiA9PT0gJ3JpZ2h0Jykge1xuICAgIC8vICAgICB0aGlzLnBvc2l0aW9uLngrKztcbiAgICAvLyB9XG4gICAgc3dpdGNoIChkaXJlY3Rpb24pIHtcbiAgICAgIGNhc2UgXCJ1cFwiOlxuICAgICAgICB0aGlzLnBvc2l0aW9uLnktLTtcbiAgICAgICAgYnJlYWs7XG4gICAgICBjYXNlIFwiZG93blwiOlxuICAgICAgICB0aGlzLnBvc2l0aW9uLnkrKztcbiAgICAgICAgYnJlYWs7XG4gICAgICBjYXNlIFwibGVmdFwiOlxuICAgICAgICB0aGlzLnBvc2l0aW9uLngtLTtcbiAgICAgICAgYnJlYWs7XG4gICAgICBjYXNlIFwicmlnaHRcIjpcbiAgICAgICAgdGhpcy5wb3NpdGlvbi54Kys7XG4gICAgICAgIGJyZWFrO1xuICAgIH1cbiAgICB0aGlzLm1ldGFib2xpc20oKTtcbiAgICB0aGlzLmRyYXcoKTtcbiAgfVxuXG4gIG1ldGFib2xpc20oKSB7XG4gICAgaWYgKHRoaXMuaHVuZ2VyIDwgMTAwKSB7XG4gICAgICB0aGlzLmh1bmdlciArPSAyO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLmhlYWx0aC0tO1xuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBNw6l0aG9kZSBxdWkgbWV0IHVuIGl0ZW0gZGFucyBsJ3NhY1xuICAgKiBAcGFyYW0ge0l0ZW19IGl0ZW0gbCdpdGVtIMOgIG1ldHRyZSBkYW5zIGxlIHNhY1xuICAgKi9cbiAgdGFrZUl0ZW0oaXRlbSkge1xuICAgIGlmICghaXRlbSkge1xuICAgICAgZm9yIChjb25zdCBpdGVyYXRvciBvZiBnYW1lT2JqZWN0cykge1xuICAgICAgICBpZiAoaXRlcmF0b3IgaW5zdGFuY2VvZiBJdGVtKSB7XG4gICAgICAgICAgaWYgKFxuICAgICAgICAgICAgdGhpcy5wb3NpdGlvbi54ID09PSBpdGVyYXRvci5wb3NpdGlvbi54ICYmXG4gICAgICAgICAgICB0aGlzLnBvc2l0aW9uLnkgPT09IGl0ZXJhdG9yLnBvc2l0aW9uLnlcbiAgICAgICAgICApIHtcbiAgICAgICAgICAgIGl0ZW0gPSBpdGVyYXRvcjtcbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgaWYgKGl0ZW0pIHtcbiAgICAgICAgdGhpcy5iYWNrcGFjay5wdXNoKGl0ZW0pO1xuICAgICAgICB0aGlzLnBvc2l0aW9uLnggPSBudWxsO1xuICAgICAgICB0aGlzLnBvc2l0aW9uLnkgPSBudWxsO1xuICAgICAgICBpdGVtLmVsZW1lbnQucmVtb3ZlKCk7XG4gICAgICB9XG4gICAgfVxuICB9XG4gIC8qKlxuICAgKiBcbiAgICogQHBhcmFtIHtJdGVtfSBpdGVtIFxuICAgKi9cbiAgdXNlSXRlbShpdGVtKXtcbiAgIGFsZXJ0KGl0ZW0udXNlKHRoaXMpKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBNw6l0aG9kZSBxdWkgY3LDqWUgbCfDqWzDqW1lbnQgSFRNTCBkdSBwZXJzb25uYWdlXG4gICAqIEByZXR1cm5zIHtFbGVtZW50fSBsJ8OpbMOpbWVudCBIVE1MIHJlcHLDqXNlbnRhbnQgbGUgcGVyc29ubmFnZVxuICAgKi9cbiAgZHJhdygpIHtcbiAgICBpZiAoIXRoaXMuZWxlbWVudCkge1xuICAgICAgdGhpcy5lbGVtZW50ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImRpdlwiKTtcbiAgICB9XG4gICAgbGV0IGRpdiA9IHRoaXMuZWxlbWVudDtcbiAgICBkaXYuaW5uZXJIVE1MID0gXCJcIjtcblxuICAgIGRpdi5jbGFzc0xpc3QuYWRkKFwiY2hhcmFjdGVyLXN0eWxlXCIpO1xuXG4gICAgbGV0IHBOYW1lID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcInBcIik7XG4gICAgcE5hbWUudGV4dENvbnRlbnQgPSBcIk5hbWUgOiBcIiArIHRoaXMubmFtZTtcbiAgICBkaXYuYXBwZW5kQ2hpbGQocE5hbWUpO1xuICAgIGxldCBwSGVhbHRoID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcInBcIik7XG4gICAgcEhlYWx0aC50ZXh0Q29udGVudCA9IFwiSGVhbHRoIDogXCIgKyB0aGlzLmhlYWx0aDtcbiAgICBkaXYuYXBwZW5kQ2hpbGQocEhlYWx0aCk7XG4gICAgbGV0IHBIdW5nZXIgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwicFwiKTtcbiAgICBwSHVuZ2VyLnRleHRDb250ZW50ID0gXCJIdW5nZXIgOiBcIiArIHRoaXMuaHVuZ2VyO1xuICAgIGRpdi5hcHBlbmRDaGlsZChwSHVuZ2VyKTtcblxuICAgIGRpdi5zdHlsZS50b3AgPSB0aGlzLnBvc2l0aW9uLnkgKiB0aWxlU2l6ZSArIFwicHhcIjtcbiAgICBkaXYuc3R5bGUubGVmdCA9IHRoaXMucG9zaXRpb24ueCAqIHRpbGVTaXplICsgXCJweFwiO1xuXG4gICAgcmV0dXJuIGRpdjtcbiAgfVxufVxuIiwiaW1wb3J0IHsgQ2hhcmFjdGVyIH0gZnJvbSBcIi4uL0NoYXJhY3RlclwiO1xuXG5leHBvcnQgY2xhc3MgQXJyb3dLZXkge1xuICAvKipcbiAgICpcbiAgICogQHBhcmFtIHtDaGFyYWN0ZXJ9IGNoYXJhY3RlciBsZSBwZXJzb25uYWdlIGF1cXVlbCBvbiB2ZXV0IGFzc2lnbmVyIGRlcyBjb250csO0bGVzIGF1IGNsYXZpZXJcbiAgICovXG4gIGNvbnN0cnVjdG9yKGNoYXJhY3Rlcikge1xuICAgIHRoaXMuY2hhcmFjdGVyID0gY2hhcmFjdGVyO1xuICB9XG5cbiAgaW5pdCgpIHtcbiAgICAvKipcbiAgICAgKiBPbiBham91dGUgdW4gZXZlbnQgYXUga2V5ZG93biBzdXIgbGEgd2luZG93IChzdXIgbGUgZG9jdW1lbnQgw6dhXG4gICAgICogbWFyY2hlIGF1c3NpKS4gT24gZmFpdCBhdHRlbnRpb24gw6AgZmFpcmUgdW5lIGZhdCBhcnJvdyBmdW5jdGlvblxuICAgICAqIHBvdXIgY29uc2VydmVyIGxhIHZhbGV1ciBkdSB0aGlzIMOgIGwnaW50w6lyaWV1ciBkZSBsJ2V2ZW50XG4gICAgICovXG4gICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoXCJrZXlkb3duXCIsIGV2ZW50ID0+IHtcbiAgICAgIC8vU2Vsb24gbGEgdG91Y2hlIHF1aSBlc3QgcHJlc3PDqWUsIG9uIGTDqWNsZW5jaGUgbGEgbcOpdGhvZGVcbiAgICAgIC8vbW92ZSBhdmVjIHVuZSB2YWxldXIgb3UgdW5lIGF1dHJlICh0ZWNobmlxdWVtZW50IG9uIHBvdXJyYWl0XG4gICAgICAvL3JlZmFjdG9yaXNlciDDp2EgdW4gcGV1IG1pZXV4LCBtYWlzIMOnYSBsZSBmZXJhKVxuICAgICAgaWYgKGV2ZW50LmtleSA9PT0gXCJBcnJvd1VwXCIpIHtcbiAgICAgICAgdGhpcy5jaGFyYWN0ZXIubW92ZShcInVwXCIpO1xuICAgICAgfVxuICAgICAgaWYgKGV2ZW50LmtleSA9PT0gXCJBcnJvd0Rvd25cIikge1xuICAgICAgICB0aGlzLmNoYXJhY3Rlci5tb3ZlKFwiZG93blwiKTtcbiAgICAgIH1cbiAgICAgIGlmIChldmVudC5rZXkgPT09IFwiQXJyb3dMZWZ0XCIpIHtcbiAgICAgICAgdGhpcy5jaGFyYWN0ZXIubW92ZShcImxlZnRcIik7XG4gICAgICB9XG4gICAgICBpZiAoZXZlbnQua2V5ID09PSBcIkFycm93UmlnaHRcIikge1xuICAgICAgICB0aGlzLmNoYXJhY3Rlci5tb3ZlKFwicmlnaHRcIik7XG4gICAgICB9XG4gICAgICBpZiAoZXZlbnQua2V5ID09PSBcIiBcIikge1xuICAgICAgICB0aGlzLmNoYXJhY3Rlci50YWtlSXRlbSgpO1xuICAgICAgfVxuICAgIH0pO1xuICB9XG59XG4iLCJpbXBvcnQgeyBDaGFyYWN0ZXIgfSBmcm9tIFwiLi4vQ2hhcmFjdGVyXCI7XG5pbXBvcnQgeyB0aWxlU2l6ZSB9IGZyb20gXCIuLi9pbmRleFwiO1xuXG5leHBvcnQgY2xhc3MgSXRlbSB7XG4gIC8qKlxuICAgKlxuICAgKiBAcGFyYW0ge3N0cmluZ30gbmFtZSBsZSBub20gZGUgbCdpdGVtXG4gICAqIEBwYXJhbSB7bnVtYmVyfSB4IHBvc2l0aW9uIHggZGUgbCdpdGVtXG4gICAqIEBwYXJhbSB7bnVtYmVyfSB5IHBvc2l0aW9uIHkgZGUgbCdpdGVtXG4gICAqIEBwYXJhbSB7c3RyaW5nfSBjc3NDbGFzcyBsYSBjbGFzc2UgY3NzIHJlcHLDqXNlbnRhbnQgY2V0IG9iamV0XG4gICAqL1xuICBjb25zdHJ1Y3RvcihuYW1lLCB4LCB5LCBjc3NDbGFzcyA9IFwiZGVmYXVsdC1jbGFzc1wiKSB7XG4gICAgdGhpcy5uYW1lID0gbmFtZTtcbiAgICB0aGlzLnBvc2l0aW9uID0ge1xuICAgICAgeCxcbiAgICAgIHlcbiAgICB9O1xuICAgIHRoaXMuY3NzQ2xhc3MgPSBjc3NDbGFzcztcbiAgICAvKipcbiAgICAgKiBAdHlwZSBFbGVtZW50XG4gICAgICovXG4gICAgdGhpcy5lbGVtZW50ID0gbnVsbDtcbiAgfVxuICAvKipcbiAgICogTcOpdGhvZGUgcXVpIHNlcmEgYXBwZWzDqWUgcXVhbmQgb24gdXRpbGlzZSBsJ2l0ZW1cbiAgICogQHBhcmFtIHtDaGFyYWN0ZXJ9IGNoYXJhY3RlciBMZSBwZXJzb25uYWdlIHF1aSB1dGlsaXNlIGwnaXRlbVxuICAgKiBAcmV0dXJucyB7c3RyaW5nfSBMYSBjaGHDrm5lIGRlIGNhcmFjdMOocmUgaW5kaXF1YW50IGwndXRpbGlzYXRpb24gZGUgbCdpdGVtXG4gICAqL1xuICB1c2UoY2hhcmFjdGVyKSB7XG4gICAgcmV0dXJuIGAke2NoYXJhY3Rlci5uYW1lfSB1c2VkICR7dGhpcy5uYW1lfSAhYDtcbiAgfVxuICAvKipcbiAgICpcbiAgICogQHJldHVybnMge0VsZW1lbnR9IGwnw6lsw6ltZW50IGh0bWwgcmVwcsOpc2VudGFudCBsJ2l0ZW1cbiAgICovXG4gIGRyYXcoKSB7XG4gICAgaWYgKCF0aGlzLmVsZW1lbnQpIHtcbiAgICAgIHRoaXMuZWxlbWVudCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIik7XG4gICAgfVxuICAgIGxldCBkaXYgPSB0aGlzLmVsZW1lbnQ7XG4gICAgZGl2LmlubmVySFRNTCA9IFwiXCI7XG5cbiAgICBkaXYuY2xhc3NMaXN0LmFkZChcIml0ZW1cIik7XG4gICAgZGl2LmNsYXNzTGlzdC5hZGQodGhpcy5jc3NDbGFzcyk7XG4gICAgZGl2LnRleHRDb250ZW50ID0gXCI/XCI7XG5cbiAgICBjb25zb2xlLmxvZyhkaXYpO1xuXG4gICAgZGl2LnN0eWxlLnRvcCA9IHRoaXMucG9zaXRpb24ueSAqIHRpbGVTaXplICsgXCJweFwiO1xuICAgIGRpdi5zdHlsZS5sZWZ0ID0gdGhpcy5wb3NpdGlvbi54ICogdGlsZVNpemUgKyBcInB4XCI7XG5cbiAgICByZXR1cm4gZGl2O1xuICB9XG59XG4iLCIvKipcbiAqIEVuIEpTIG1vZHVsYWlyZSwgcG91ciBwb3V2b2lyIHV0aWxpc2VyIGxhIGNsYXNzZSBDaGFyYWN0ZXJcbiAqIGRhbnMgbm90cmUgZmljaGllciBpbmRleCwgb24gZG9pdCBkJ2Fib3JkIGltcG9ydGVyIGxhXG4gKiBjbGFzc2UgZW4gcXVlc3Rpb24gZGUgc29uIGZpY2hpZXJcbiAqL1xuaW1wb3J0IHsgQ2hhcmFjdGVyIH0gZnJvbSBcIi4vQ2hhcmFjdGVyXCI7XG5pbXBvcnQgeyBBcnJvd0tleSB9IGZyb20gXCIuL2NvbnRyb2xzL0Fycm93S2V5XCI7XG5pbXBvcnQgeyBJdGVtIH0gZnJvbSBcIi4vY29udHJvbHMvSXRlbVwiO1xuXG5leHBvcnQgY29uc3QgdGlsZVNpemUgPSAxMDA7XG4vL09uIGZhaXQgdW5lIGluc3RhbmNlIGRlIHBlcnNvbm5hZ2VcbmxldCBwZXJzbzEgPSBuZXcgQ2hhcmFjdGVyKFwiUGVyc28gMVwiKTtcbmV4cG9ydCBjb25zdCBnYW1lT2JqZWN0cyA9IFtcbiAgcGVyc28xLFxuICBuZXcgSXRlbShcImxpdHRsZSBoYXRcIiwgNCwgMiksXG4gIG5ldyBJdGVtKFwiYnJlYWRcIiwgNSwgNSlcbl07XG5mb3IgKGNvbnN0IGl0ZXJhdG9yIG9mIGdhbWVPYmplY3RzKSB7XG4gIGRvY3VtZW50LmJvZHkuYXBwZW5kQ2hpbGQoaXRlcmF0b3IuZHJhdygpKTtcbn1cblxucGVyc28xLnVzZUl0ZW0obmV3IEl0ZW0oJ3NvbWUgc3R1ZmYnKSk7XG4vL29uIGZhaXQgdW5lIGluc3RhbmNlIGRlcyBjb250csO0bGVzIGF1IGNsYXZpZXIgZW4gbHVpXG4vL2Rvbm5hbnQgbCdpbnN0YW5jZSBkZSBwZXJzb25uYWdlIMOgIGNvbnRyw7RsZXJcbmxldCBhcnJvd0tleSA9IG5ldyBBcnJvd0tleShwZXJzbzEpO1xuLy9PbiBpbml0aWFsaXNlIGxlcyBjb250csO0bGVzIGF1IGNsYXZpZXJcbmFycm93S2V5LmluaXQoKTtcbi8vT24gZmFpdCB1biBkcmF3IGluaXRpYWwgcG91ciBhcHBlbmQgbGUgaHRtbCBkdSBwZXJzb25uYWdlIG/DuSBvbiB2ZXV0XG4iXSwic291cmNlUm9vdCI6IiJ9