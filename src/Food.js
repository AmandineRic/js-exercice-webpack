import { Item } from "./controls/Item";

export class Food extends Item {

    /**
     * 
     * @param {string} name Foods name
     * @param {number} x Foods position
     * @param {number} y Foods position
     * @param {number} calories Foods calories
     */
    constructor(name, x, y, calories){
        super(name, x, y, 'food');
        this.calories = calories;
    }

    /**
     * 
     * @param {Character} character  
     */
    use(character){
        character.hunger -= this.calories;
        return super.use(character);
    }
}

let food = new Food('apple');