import { tileSize, gameObjects } from "./index";
import { Item } from "./controls/Item";

/**
 * Pour que cette classe soit importable dans un autre
 * fichier, on doit l'indiquer avec le mot clef export
 */
export class Character {
  /**
   * @param {string} name
   */
  constructor(name) {
    this.name = name;
    this.health = 100;
    this.hunger = 0;
    this.position = {
      x: 0,
      y: 0
    };
    /**
     * @type Element
     */
    this.element = null;
    /**
     * @type Item[]
     */
    this.backpack = [];
  }
  /**
   * Méthode qui déplace le personnage dans une direction donnée
   * @param {string} direction la direction dans laquelle va le personnage (doit être up, down, left ou right)
   */
  move(direction) {
    // if (direction === 'up') {
    //     this.position.y--;
    // }
    // if (direction === 'down') {
    //     this.position.y++;
    // }
    // if (direction === 'left') {
    //     this.position.x--;
    // }
    // if (direction === 'right') {
    //     this.position.x++;
    // }
    switch (direction) {
      case "up":
        this.position.y--;
        break;
      case "down":
        this.position.y++;
        break;
      case "left":
        this.position.x--;
        break;
      case "right":
        this.position.x++;
        break;
    }
    this.metabolism();
    this.draw();
  }

  metabolism() {
    if (this.hunger < 100) {
      this.hunger += 2;
    } else {
      this.health--;
    }
  }

  /**
   * Méthode qui met un item dans l'sac
   * @param {Item} item l'item à mettre dans le sac
   */
  takeItem(item) {
    if (!item) {
      for (const iterator of gameObjects) {
        if (iterator instanceof Item) {
          if (
            this.position.x === iterator.position.x &&
            this.position.y === iterator.position.y
          ) {
            item = iterator;
            break;
          }
        }
      }
      if (item) {
        this.backpack.push(item);
        this.position.x = null;
        this.position.y = null;
        item.element.remove();
      }
    }
  }
  /**
   * 
   * @param {Item} item 
   */
  useItem(item){
   alert(item.use(this));
  }

  /**
   * Méthode qui crée l'élément HTML du personnage
   * @returns {Element} l'élément HTML représentant le personnage
   */
  draw() {
    if (!this.element) {
      this.element = document.createElement("div");
    }
    let div = this.element;
    div.innerHTML = "";

    div.classList.add("character-style");

    let pName = document.createElement("p");
    pName.textContent = "Name : " + this.name;
    div.appendChild(pName);
    let pHealth = document.createElement("p");
    pHealth.textContent = "Health : " + this.health;
    div.appendChild(pHealth);
    let pHunger = document.createElement("p");
    pHunger.textContent = "Hunger : " + this.hunger;
    div.appendChild(pHunger);

    div.style.top = this.position.y * tileSize + "px";
    div.style.left = this.position.x * tileSize + "px";

    return div;
  }
}
